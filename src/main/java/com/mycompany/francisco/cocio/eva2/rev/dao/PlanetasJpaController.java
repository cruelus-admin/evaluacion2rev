/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.francisco.cocio.eva2.rev.dao;

import com.mycompany.francisco.cocio.eva2.rev.dao.exceptions.NonexistentEntityException;
import com.mycompany.francisco.cocio.eva2.rev.dao.exceptions.PreexistingEntityException;
import com.mycompany.francisco.cocio.eva2.rev.entity.Planetas;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author franc
 */
public class PlanetasJpaController implements Serializable {

    public PlanetasJpaController() {
     
    }
    private  EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Planetas planetas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(planetas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPlanetas(planetas.getCodPlaneta()) != null) {
                throw new PreexistingEntityException("Planetas " + planetas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Planetas planetas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            planetas = em.merge(planetas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = planetas.getCodPlaneta();
                if (findPlanetas(id) == null) {
                    throw new NonexistentEntityException("The planetas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Planetas planetas;
            try {
                planetas = em.getReference(Planetas.class, id);
                planetas.getCodPlaneta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The planetas with id " + id + " no longer exists.", enfe);
            }
            em.remove(planetas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Planetas> findPlanetasEntities() {
        return findPlanetasEntities(true, -1, -1);
    }

    public List<Planetas> findPlanetasEntities(int maxResults, int firstResult) {
        return findPlanetasEntities(false, maxResults, firstResult);
    }

    private List<Planetas> findPlanetasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Planetas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Planetas findPlanetas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Planetas.class, id);
        } finally {
            em.close();
        }
    }

    public int getPlanetasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Planetas> rt = cq.from(Planetas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
