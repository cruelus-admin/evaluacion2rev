<%-- 
    Document   : editar
    Created on : 16-10-2021, 18:59:47
    Author     : franc
--%>

<%@page import="com.mycompany.francisco.cocio.eva2.rev.entity.Planetas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Planetas planet = (Planetas) request.getAttribute("planeta");
    

%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>Edita un planeta</title>
    </head>
    <body>
        
        <h2>EDITA UN PLANETA</h2>
        <form name="formulario" action="PlanetasController" method="POST">
            <div class="form-group">
                <label for="codPlaneta">Codigo Planetario:</label>
                <input type="number" class="form-control" name="cod_planeta" id="cod_planeta" value="<%= planet.getCodPlaneta()%>">
            </div>
            <div class="form-group">
                <label for="nombrePlaneta">Nombre del Planeta:</label>
                <input type="text" class="form-control" name="nombre_planeta" id="nombre_planeta" value="<%= planet.getNombrePlaneta()%>">
            </div>
            <div class="form-group">
                <label for="Region">Región Galáctica:</label>
                <input type="text" class="form-control" name="region_galactica" id="region_galactica" value="<%= planet.getRegionGalactica()%>">
            </div>
            <div class="form-group">
                <label for="Afiliacion">Afiliacion:</label>
                <input type="text" class="form-control" name="afiliacion" id="afiliacion" value="<%= planet.getAfiliacion()%>">
            </div>
            <div class="form-group">
                <label for="Poblacion">Poblacion:</label>
                <input type="text" class="form-control" name="poblacion" id="poblacion" value="<%= planet.getPoblacion()%>">
            </div>
            <button type="submit" name="accion" value="grabarcambios" class="btn btn-info">Actualizar</button>
        </form>
        
    </body>
</html>
