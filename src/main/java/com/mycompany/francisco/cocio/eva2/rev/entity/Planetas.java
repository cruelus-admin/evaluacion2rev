/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.francisco.cocio.eva2.rev.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franc
 */
@Entity
@Table(name = "planetas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Planetas.findAll", query = "SELECT p FROM Planetas p"),
    @NamedQuery(name = "Planetas.findByCodPlaneta", query = "SELECT p FROM Planetas p WHERE p.codPlaneta = :codPlaneta"),
    @NamedQuery(name = "Planetas.findByNombrePlaneta", query = "SELECT p FROM Planetas p WHERE p.nombrePlaneta = :nombrePlaneta"),
    @NamedQuery(name = "Planetas.findByRegionGalactica", query = "SELECT p FROM Planetas p WHERE p.regionGalactica = :regionGalactica"),
    @NamedQuery(name = "Planetas.findByAfiliacion", query = "SELECT p FROM Planetas p WHERE p.afiliacion = :afiliacion"),
    @NamedQuery(name = "Planetas.findByPoblacion", query = "SELECT p FROM Planetas p WHERE p.poblacion = :poblacion")})
public class Planetas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "cod_planeta")
    private String codPlaneta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_planeta")
    private String nombrePlaneta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "region_galactica")
    private String regionGalactica;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "afiliacion")
    private String afiliacion;
    @Size(min = 1, max = 2147483647)
    @NotNull
    @Column(name = "poblacion")
    private String poblacion;

    public Planetas() {
    }

    public Planetas(String codPlaneta) {
        this.codPlaneta = codPlaneta;
    }

    public Planetas(String codPlaneta, String nombrePlaneta, String regionGalactica, String afiliacion) {
        this.codPlaneta = codPlaneta;
        this.nombrePlaneta = nombrePlaneta;
        this.regionGalactica = regionGalactica;
        this.afiliacion = afiliacion;
    }

    public String getCodPlaneta() {
        return codPlaneta;
    }

    public void setCodPlaneta(String codPlaneta) {
        this.codPlaneta = codPlaneta;
    }

    public String getNombrePlaneta() {
        return nombrePlaneta;
    }

    public void setNombrePlaneta(String nombrePlaneta) {
        this.nombrePlaneta = nombrePlaneta;
    }

    public String getRegionGalactica() {
        return regionGalactica;
    }

    public void setRegionGalactica(String regionGalactica) {
        this.regionGalactica = regionGalactica;
    }

    public String getAfiliacion() {
        return afiliacion;
    }

    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPlaneta != null ? codPlaneta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Planetas)) {
            return false;
        }
        Planetas other = (Planetas) object;
        if ((this.codPlaneta == null && other.codPlaneta != null) || (this.codPlaneta != null && !this.codPlaneta.equals(other.codPlaneta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.francisco.cocio.eva2.rev.entity.Planetas[ codPlaneta=" + codPlaneta + " ]";
    }
    
}
