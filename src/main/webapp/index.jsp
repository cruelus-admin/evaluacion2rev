<%-- 
    Document   : index
    Created on : 16-10-2021, 18:59:47
    Author     : franc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>Agrega un planeta</title>
    </head>
    <body>
        
        <h2>INGRESE UN PLANETA</h2>
        <form name="formulario" action="PlanetasController" method="POST">
            <div class="form-group">
                <label for="codPlaneta">Codigo Planetario:</label>
                <input type="number" class="form-control" name="cod_planeta" id="cod_planeta">
            </div>
            <div class="form-group">
                <label for="nombrePlaneta">Nombre del Planeta:</label>
                <input type="text" class="form-control" name="nombre_planeta" id="nombre_planeta">
            </div>
            <div class="form-group">
                <label for="Region">Región Galáctica:</label>
                <input type="text" class="form-control" name="region_galactica" id="region_galactica">
            </div>
            <div class="form-group">
                <label for="Afiliacion">Afiliacion:</label>
                <input type="text" class="form-control" name="afiliacion" id="afiliacion">
            </div>
            <div class="form-group">
                <label for="Poblacion">Poblacion:</label>
                <input type="text" class="form-control" name="poblacion" id="poblacion">
            </div>
            <button type="submit" name="accion" value="grabar" class="btn btn-primary">Grabar</button>
        </form>
        
    </body>
</html>
