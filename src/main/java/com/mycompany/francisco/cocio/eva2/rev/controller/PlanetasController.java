/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.francisco.cocio.eva2.rev.controller;

import com.mycompany.francisco.cocio.eva2.rev.dao.PlanetasJpaController;
import com.mycompany.francisco.cocio.eva2.rev.dao.exceptions.NonexistentEntityException;
import com.mycompany.francisco.cocio.eva2.rev.entity.Planetas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author franc
 */
@WebServlet(name = "PlanetasController", urlPatterns = {"/PlanetasController"})
public class PlanetasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PlanetasController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PlanetasController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String botonaccion = request.getParameter("accion");
        PlanetasJpaController dao = new PlanetasJpaController();

        if (botonaccion.equals("crear")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } //fin if crear
        else if (botonaccion.equals("eliminar")) {

            try {
                String select = request.getParameter("select");
                dao.destroy(select);

                List<Planetas> listaPlanetas = dao.findPlanetasEntities();
                request.setAttribute("Lista Planetas", listaPlanetas);
                request.getRequestDispatcher("listaplanetas.jsp").forward(request, response);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(PlanetasController.class.getName()).log(Level.SEVERE, null, ex);
            }//fin trycatch

        }//fin if eliminar
        else {
            String select = request.getParameter("select");
            Planetas planet = dao.findPlanetas(select);

            request.setAttribute("planeta", planet);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        } //fin if editar

//        processRequest(request, response);
    }//fin get

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String botonaccion = request.getParameter("accion");
        PlanetasJpaController dao = new PlanetasJpaController();

        try {

            String codPlaneta = request.getParameter("cod_planeta");
            String nombrePlaneta = request.getParameter("nombre_planeta");
            String region = request.getParameter("region_galactica");
            String afiliacion = request.getParameter("afiliacion");
            String poblacion = request.getParameter("poblacion");

            Planetas nuevoplaneta = new Planetas();
            nuevoplaneta.setCodPlaneta(codPlaneta);
            nuevoplaneta.setNombrePlaneta(nombrePlaneta);
            nuevoplaneta.setRegionGalactica(region);
            nuevoplaneta.setAfiliacion(afiliacion);
            nuevoplaneta.setPoblacion(poblacion);

            if (botonaccion.equals("grabar")) {

                dao.create(nuevoplaneta);
            }//end 
            else {
                dao.edit(nuevoplaneta);

            }//end if 

//            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PlanetasController.class.getName()).log(Level.SEVERE, null, ex);
        }//fin try catch

//envio datos a lista planetas        
        List<Planetas> listaPlanetas = dao.findPlanetasEntities();
        request.setAttribute("Lista Planetas", listaPlanetas);
        request.getRequestDispatcher("listaplanetas.jsp").forward(request, response);

    }//fin POST

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
