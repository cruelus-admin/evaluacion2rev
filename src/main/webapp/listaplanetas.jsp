<%-- 
    Document   : listaplanetas
    Created on : 16-10-2021, 20:01:37
    Author     : franc
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.francisco.cocio.eva2.rev.entity.Planetas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Planetas> lista = (List<Planetas>) request.getAttribute("Lista Planetas");
    Iterator<Planetas> itlista = lista.iterator();

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>Lista Planetas</title>
    </head>


    <body>
        <form name="formulario" action="PlanetasController" method="GET">       

            <div class="container">
                <h2>Lista de planetas</h2>         
                <table class="table">
                    <thead>
                        <tr>
                            <th>Codigo Planeta</th>
                            <th>Nombre Planeta</th>
                            <th>Región Galáctica</th>
                            <th>Afiliacion</th>
                            <th>Poblacion</th>
                            <th>Seleccion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  while (itlista.hasNext()) {
              Planetas plane = itlista.next();%>
                        <tr>
                            <td><%= plane.getCodPlaneta()%></td>
                            <td><%= plane.getNombrePlaneta()%></td>
                            <td><%= plane.getRegionGalactica()%></td>
                            <td><%= plane.getAfiliacion()%></td>
                            <td><%= plane.getPoblacion()%></td>
                            <td><input type="radio" name="select" value="<%= plane.getCodPlaneta()%>" ></td>
                        </tr> <%}%>

                    </tbody>
                </table>
            </div>

            <button type="submit" name="accion" class="btn btn-primary"value="crear">Crear Planeta</button>   
            <button type="submit" name="accion" class="btn btn-info"value="editar">Editar Planeta</button>  
            <button type="submit" name="accion" class="btn btn-danger"value="eliminar">Eliminar Planeta</button>  
        </form>    




    </body>
</html>
